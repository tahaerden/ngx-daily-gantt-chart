import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ngx-daily-gantt-chart';
  tasks = [
    {
      id: 1,
      label: 'task 1',
      description: 'description for task 1',
      start: '09:00',
      end: '10:00'
    },
    {
      id: 2,
      label: 'task 2',
      description: 'description for task 2',
      start: '10:00',
      end: '11:00',
      isParent: true
    },
    {
      id: 3,
      parentID: 2,
      isHidden: true,
      label: 'task 2a',
      description: 'description for task 2a',
      start: '10:00',
      end: '14:25'
    },
    {
      id: 4,
      label: 'task 4',
      description: 'description for task 4',
      start: '10:23',
      end: '12:20'
    },
    {
      id: 5,
      label: 'task 5',
      description: 'description for task 5',
      start: '14:10',
      end: '14:30'
    },
    {
      id: 6,
      label: 'task 6',
      description: 'description for task 6',
      start: '10:00',
      end: '11:00'
    },
    {
      id: 7,
      label: 'task 7',
      description: 'description for task 7',
      start: '10:00',
      end: '11:00'
    },
    {
      id: 8,
      label: 'task 8',
      description: 'description for task 8',
      start: '10:00',
      end: '11:00'
    },
    {
      id: 9,
      label: 'task 9',
      description: 'description for task 9',
      start: '10:00',
      end: '11:00'
    },
    {
      id: 10,
      label: 'task 10',
      description: 'description for task 10',
      start: '10:00',
      end: '11:00'
    },
    {
      id: 11,
      label: 'task 11',
      description: 'description for task 11',
      start: '10:00',
      end: '11:00'
    },
    {
      id: 12,
      label: 'task 12',
      description: 'description for task 12',
      start: '10:00',
      end: '11:00'
    },
    {
      id: 13,
      label: 'task 13',
      description: 'description for task 13',
      start: '10:00',
      end: '11:00'
    },
    {
      id: 14,
      label: 'task 14',
      description: 'description for task 14',
      start: '10:00',
      end: '11:00'
    },
    {
      id: 15,
      label: 'task 15',
      description: 'description for task 15',
      start: '10:00',
      end: '11:00'
    }
  ];
}
